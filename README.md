# TV-Rights
## Description
Microservice to manage rights of users of the TV Project.
## API
### Roles
#### GET on /roles/check
Send these params to /roles/check
```json
{
  "url": "regex",
  "method": "regex",
  "roles": ["role"]
}
```
You will get a 403 response if you don't have rights, else 204.
