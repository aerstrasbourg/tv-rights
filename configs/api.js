var express = require('express');
var bodyParser = require('body-parser');
var app = express();
exports.app = app;
var rolesRouter = require('../lib/roles');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use('/roles', rolesRouter);
