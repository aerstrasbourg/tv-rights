var mongoose = require('mongoose');
var app = require('./api').app;

var envIp = process.env.RIGHTS_DATABASE_IP;
var db = "rights";
var ip = envIp === undefined ? "localhost" : envIp;

exports.connect = function(done) {
  console.log('Connecting to ' + ip + '.');
  mongoose.connect('mongodb://' + ip + '/' + db);

  var conn = mongoose.connection;
  app.set('conn', conn);

  conn.once('open', function () {
    console.log('Connected.');
    if (done)
      done();
  });
};

exports.disconnect = function() {
  mongoose.connection.close();
};
