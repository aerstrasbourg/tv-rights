var mongoose = require('mongoose');

var RoleRightsSchema = mongoose.Schema({
  role: String,
  rights: [{
    method: String,
    url: String
  }]
});

var RoleRights = mongoose.model('RoleRights', RoleRightsSchema);

module.exports = RoleRights;
