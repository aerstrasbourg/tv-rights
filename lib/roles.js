var express = require('express');
var app = require('../configs/api').app;
var db = require('../configs/database');

var RoleRights = require('./models/rolerights');

var router = express.Router();

router.get('/check', function(req, res) {
  if (!req.body.url || !req.body.method || !req.body.roles) {
    res.status(400);
    res.end();
  } else {
    var re = new RegExp(req.body.roles.join('|'), 'i');
    RoleRights.find({role: re}, function(err, rolerights) {
      if (err)
        res.status(500);
      else {
        if (!rolerights)
          res.status(403);
        else {
          res.status(204);
          var b = false;
          for (var i = 0; i < rolerights.length && b === false; ++i) {
            var role = rolerights[i];
            for (var j = 0; j < role.rights.length && b === false; ++j) {
              var right = role.rights[j];
              if (req.body.url.match(new RegExp(right.url)) &&
                  req.body.method.match(new RegExp(right.method))) {
                b = true;
              }
            }
          }
          if (b === true)
            res.status(204);
          else
            res.status(403);
        }
        res.end();
      }
    });
  }
});

module.exports = router;
