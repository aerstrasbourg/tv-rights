var should = require('should');
var request = require('supertest');
var api = require('../configs/api').app;
var db = require('../configs/database');

var RoleRights = require('../lib/models/rolerights');

describe('/roles', function() {
  before(function(done) {
    db.connect(done);
  });

  after(function() {
    db.disconnect();
  });

  describe('/auth', function() {
    before(function() {
      var aerRights = new RoleRights({
        role: 'aer',
        rights: [{
          method: '.*',
          url: '/thomas/.*'
        }, {
          method: 'post',
          url: '/test/rights'
        }]
      });
      aerRights.save();
      var admRights = new RoleRights({
        role: 'adm',
        rights: [{
          method: '.*',
          url: '/thomas/.*'
        }, {
          method: '.*',
          url: '.*'
        }]
      });
      admRights.save();
      var rootRights = new RoleRights({
        role: 'root',
        rights: [{
          method: 'post',
          url: '/test/.*'
        }, {
          method: 'get',
          url: '/test/rights'
        }]
      });
      rootRights.save();
      var studentRights = new RoleRights({
        role: 'student',
        rights: [{
          method: 'delete',
          url: '/thomas/.*'
        }]
      });
      studentRights.save();
    });

    after(function() {
      RoleRights.find({}).remove();
    });

    it('should successfuly respond it has rights', function(done) {
      request(api)
        .get('/roles/check')
        .send({
          url: "/test/rights",
          method: "get",
          roles: [
            "adm",
            "aer"
          ]
        })
        .expect(204)
        .end(function(err, res) {
          if (err)
            return done(err);
          done();
        });
    });
  });
});
